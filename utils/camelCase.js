const camelcaseKeys = require('camelcase-keys');

const camelCaseObject = source => {
  if (source instanceof Array) {
    return source.map(camelCaseObject);
  } else {
    return camelcaseKeys(source, {deep: true});
  }
};

module.exports = camelCaseObject;
