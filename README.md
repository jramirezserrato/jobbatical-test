# Jobbatical Test
## By Jorge Ramírez Serrato

### Tech Stack
1. **Hapi** to power-up the web server
2. **Joi** as request validation library
3. **Mocha, Chai and Sinon** as test runner, assertions library and mock library
4. **Knex** as a data access library
5. **PostgresSQL** database

To start server

```
npm install
npm start
```

To run test suite

```
npm test
```

Enjoy!
