const { queries } = require('../config');

const users = (db) => {
  const find = ({id}) => {
    // I decided to use raw queries because I found out they can take almost 50% less time than composed queries with knex (or any ORM)
    // The caveat is this is difficult to maintain, but given the goal of the assigment, I chose performance over maintanability
    return db.raw(`
      with applications as (
          select a.id, a.created_at, a.cover_letter, a.user_id, a.listing_id,
                coalesce(row_to_json((select x from (select l.id, l.name, l.description) x)), '{}') as listing
          from applications a
          inner join listings l on a.listing_id = l.id
          group by a.id, a.created_at, a.cover_letter, l.id, l.name, l.description
      ), createdListings as (
          select l.id, l.created_at, l.name, l.description, a.user_id
          from listings l
          inner join applications a on a.listing_id = l.id
      ), companies as (
          select c.id, c.created_at, c.name, t.user_id, t.contact_user as is_contact
          from companies c
          inner join teams t on t.company_id = c.id
      )
      select u.id, u.name, u.created_at,
            coalesce((select json_agg(x)
                      from (select c.id, c.name, c.created_at, c.is_contact
                            from companies c
                            where c.user_id = u.id
                            order by c.created_at desc
                            limit ${queries.connectedResourcesLimit}) x), '[]') as companies,
            coalesce((select json_agg(x)
                      from (select a.id, a.created_at, a.listing, a.cover_letter
                            from applications a
                            where a.user_id = u.id
                            order by a.created_at desc
                            limit ${queries.connectedResourcesLimit}) x), '[]') as applications,
            coalesce((select json_agg(x)
                      from (select cl.id, cl.name, cl.description, cl.created_at
                            from createdListings cl
                            where cl.user_id = u.id
                            order by cl.created_at desc
                            limit ${queries.connectedResourcesLimit}) x), '[]') as created_listings
      from users u
      where u.id = ${id}
      group by u.id, u.name, u.created_at
    `).then(result => result.rows.shift());
  };

  return {
    find
  };
};

module.exports = users;
