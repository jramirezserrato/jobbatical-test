const { database } = require('../config');

// Workaround to get bigint as integer from pg
const pg = require('pg');
pg.types.setTypeParser(20, 'text', parseInt);

const db = require('knex')({
  client: 'pg',
  connection: {
    host: database.host,
    port: database.port,
    user: database.user,
    password: database.password,
    database: database.name
  },
  pool: { min: 0, max: 7 }
});

module.exports = {
  topActiveUsers: require('./topActiveUsers')(db),
  users: require('./users')(db)
};
