const totalActiveUsers = (db) => {
  const search = ({pageSize = 10, pageNumber = 1}) => {
    // I decided to use raw queries because I found out they can take almost 50% less time than composed queries with knex (or any ORM)
    // The caveat is this is difficult to maintain, but given the goal of the assigment, I chose performance over maintanability
    return db.raw(`
      with listings as (
        select l.id, l.name, l.created_at, a.user_id
        from listings l
        inner join applications a on a.listing_id = l.id
        where a.created_at >= date_trunc('week', CURRENT_TIMESTAMP - interval '1 week')
      )
      select u.id, u.created_at, u.name, count(a.id) as count,
              coalesce((select json_agg(x)
                        from (select l.id, l.name
                              from listings l
                              where l.user_id = u.id
                              order by l.created_at desc
                              limit 3) x), '[]') as listings
      from users u
      left join applications a on a.user_id = u.id and a.created_at >= date_trunc('week', CURRENT_TIMESTAMP - interval '1 week')
      group by u.id, u.created_at, u.name
      order by count desc, u.name
      offset ${(pageNumber - 1) * pageSize}
      limit ${pageSize};
    `).then(result => result.rows);
  };

  return {
    search
  };
};

module.exports = totalActiveUsers;
