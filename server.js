const Hapi = require('hapi');
const chalk = require('chalk');
const requireDir = require('require-dir');

const config = require('./config');
const camelCaseObject = require('./utils/camelCase');

const server = new Hapi.Server();

server.connection({
  port: config.port,
  routes: {
    cors: true
  }
});

// Load routes
const routes = requireDir('./routes');
for (let route in routes) {
  if (config.env === 'development') {
    console.log(chalk.blue(`Loaded route: ${route.replace('js','')}`));
  }

  server.route(routes[route]);
}

// Hide controlled errors stack when doing unit tests
if (config.env === 'test') {
  server.settings.debug = false;
}

// Log requests
server.on('response', (request) => {
  if (config.env !== 'test') {
    console.log(chalk.yellow(`${request.info.remoteAddress}:${request.method.toUpperCase()} ${request.url.path} --> ${request.response.statusCode}`));
  }
});

// Camel case middleware
server.ext('onPreResponse', (request, reply) => {
    if (request.response.statusCode === 200) {
      request.response.source = camelCaseObject(request.response.source);
    }
    return reply.continue();
});

// Start the server
if (!module.parent) {
  server.start((err) => {
    if (err) {
      throw err;
    }

    console.info(chalk.green('Server running at:', server.info.uri));
  });
}

module.exports = server;
