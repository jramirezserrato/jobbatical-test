const env = process.env.NODE_ENV || 'development';

// All configurations will extend these options
const all = {
	env: env,

	// Server port
	port: process.env.PORT || 3000
};

// Export the config object based on the NODE_ENV
let config = Object.assign({},
	all,
	require('./development')
);

// Development will be our base config
if (env !== 'development') {
	config = Object.assign(config, require('./' + env + '.js') || {});
}

module.exports = config;
