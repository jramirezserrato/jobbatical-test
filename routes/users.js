const Joi = require('joi');

const db = require('../repositories');

module.exports = [{
  method: 'GET',
  path: '/users',
  handler: (request, reply) => {
    const id = request.query.id;

    return db.users.find({ id }).then((user) => {
      if (!user) {
        return reply({ message: 'User not found' }).code(404);
      }

      return reply(user);
    }).catch((e) => {
      console.log(e);
      return reply({ message: 'Internal Server Error' }).code(500);
    });
  },
  config: {
    validate: {
      query: {
        id: Joi.number().integer().required()
      }
    }
  }
}];
