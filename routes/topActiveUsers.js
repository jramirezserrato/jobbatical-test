const Joi = require('joi');

const db = require('../repositories');

module.exports = [{
  method: 'GET',
  path: '/topActiveUsers',
  handler: (request, reply) => {
    const pageNumber = request.query.page;

    return db.topActiveUsers.search({ pageNumber }).then((users) => {

      //Map listings returned from DB to match the spec
      const responseBody = users.map(user => {
        const response = Object.assign({}, user);
        response.listings = response.listings.map(listing => listing.name);
        return response;
      });

      return reply(responseBody);
    }).catch(() => {
      return reply({ message: 'Internal Server Error' }).code(500);
    });
  },
  config: {
    validate: {
      query: {
        page: Joi.number().integer().greater(0).default(1)
      }
    }
  }
}];
