const expect = require('chai').expect;

const camelCase = require('../../utils/camelCase');

describe('Camel case utility', () => {
  describe('When a single level object is passed', () => {
    it('Should return all keys camel cased', () => {
      const input = {
        first_name: 'Jorge',
        last_name: 'Ramirez',
        created_by: 1
      };

      const expected = {
        firstName: 'Jorge',
        lastName: 'Ramirez',
        createdBy: 1
      };

      const result = camelCase(input);

      expect(result).to.deep.equal(expected);
    });
  });

  describe('When a nested object is passed', () => {
    it('Should return all keys camel cased', () => {
      const input = {
        first_name: 'Jorge',
        last_name: 'Ramirez',
        created_by: 1,
        address: {
          actual_city: 'Cartagena',
          country: 'Colombia'
        }
      };

      const expected = {
        firstName: 'Jorge',
        lastName: 'Ramirez',
        createdBy: 1,
        address: {
          actualCity: 'Cartagena',
          country: 'Colombia'
        }
      };

      const result = camelCase(input);

      expect(result).to.deep.equal(expected);
    });
  });

  describe('When an array of objects is passed', () => {
    it('Should return all keys camel cased', () => {
      const input = [
        { full_name: 'Jorge Ramirez', phone_number: '12345' },
        { full_name: 'John Doe', phone_number: '12345' }
      ];

      const expected = [
        { fullName: 'Jorge Ramirez', phoneNumber: '12345' },
        { fullName: 'John Doe', phoneNumber: '12345' }
      ];

      const result = camelCase(input);

      expect(result).to.deep.equal(expected);
    });
  });

  describe('When nested arrays of objects is passed', () => {
    it('Should return all keys camel cased', () => {
      const input = [
        { full_name: 'Jorge Ramirez', phone_number: '12345', companies: [ { id: 1, is_contact: true } ] },
        { full_name: 'John Doe', phone_number: '12345', companies: [ { id: 2, is_contact: false } ] }
      ];

      const expected = [
        { fullName: 'Jorge Ramirez', phoneNumber: '12345', companies: [ { id: 1, isContact: true } ] },
        { fullName: 'John Doe', phoneNumber: '12345', companies: [ { id: 2, isContact: false } ] }
      ];

      const result = camelCase(input);

      expect(result).to.deep.equal(expected);
    });
  });
});

