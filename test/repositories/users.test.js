const expect = require('chai').expect;

const users = require('../../repositories/users');

describe('Users Repository', () => {
  describe('find', () => {
    it('Should return an object when id is found', (done) => {
      const dbMock = {
        raw: () => Promise.resolve({
          rows: [{
            "id": 3,
            "created_at": "2016-01-01T15:30:00.000Z",
            "name": "Juan",
            "count": 1,
          }]
        })
      };

      users(dbMock).find({ id: 1 }).then((result) => {
        expect(result).to.exist;
        expect(result).to.be.an('object');
        done();
      });
    });

    it('Should return an undefined when nothing is found', (done) => {
      const dbMock = {
        raw: () => Promise.resolve({
          rows: []
        })
      };

      users(dbMock).find({ id: 1 }).then((result) => {
        expect(result).to.not.exist;
        done();
      });
    });
  });
});
