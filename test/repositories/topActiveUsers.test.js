const expect = require('chai').expect;

const topActiveUsers = require('../../repositories/topActiveUsers');

describe('Top active users Repository', () => {
  describe('search', () => {
    const dbMock = {
      raw: () => Promise.resolve({
        rows: [{
          "id": 3,
          "created_at": "2016-01-01T15:30:00.000Z",
          "name": "Juan",
          "count": 1,
          "listings": [{ "id": 1, "name": "Listing" }]
        }]
      })
    };

    it('Should return an array of data', (done) => {
      topActiveUsers(dbMock).search({}).then((result) => {
        expect(result).to.exist;
        expect(result).to.be.instanceof(Array);
        done();
      });
    });
  });
});
