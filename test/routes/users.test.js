const server = require('../../server.js');
const expect = require('chai').expect;
const sinon = require('sinon');

const db = require('../../repositories');

describe('Users resource', () => {
  describe('GET /users?id={id}', () => {
    const options = {
      method: 'GET',
      url: '/users?id=1'
    };

    const validDBData = {
      id: 1,
      name: 'Jorge',
      created_at: '2016-01-01T15:30:00.000Z',
      companies: [{
        id: 2,
        created_at: '2016-01-01T15:30:00.000Z',
        name: 'Jorge Inc.',
        isContact: true
      }],
      created_listings: [{
        id: 2,
        created_at: '2016-01-01T15:30:00.000Z',
        name: 'My listing',
        description: 'Listing description'
      }],
      applications: [{
        id: 1,
        created_at: '2016-01-01T15:30:00.000Z',
        listing: {
          id: 2,
          name: 'My listing',
          description: 'Listing description'
        },
        cover_letter: 'A letter'
      }]
    };

    let findUserStub;

    beforeEach(() => {
      findUserStub = sinon.stub(db.users, 'find');
    });

    afterEach(() => {
      findUserStub.restore();
    });

    describe('When request is successful', () => {
      it('Should return a HTTP 200 code', (done) => {
        findUserStub.returns(Promise.resolve(validDBData));

        server.inject(options, (response) => {
          expect(response.statusCode).to.equal(200);
          done();
        });
      });

      it('Should return a single object', (done) => {
        findUserStub.returns(Promise.resolve(validDBData));

        server.inject(options, (response) => {
          expect(response.result).to.be.instanceof(Object);
          done();
        });
      });
    });

    describe('When resource is not found', () => {
      it('Should return a HTTP 404 code', (done) => {
        findUserStub.returns(Promise.resolve());

        server.inject(options, (response) => {
          expect(response.statusCode).to.equal(404);
          done();
        });
      });
    });

    describe('When user id is not passed', () => {
      it('Should return a HTTP 400 code', (done) => {
        const newOptions = Object.assign({}, options, { url: '/users' });

        server.inject(newOptions, (response) => {
          expect(response.statusCode).to.equal(400);
          done();
        });
      });
    });

    describe('When an error is thrown', () => {
      it('Should return a HTTP 500 code', (done) => {
        findUserStub.returns(Promise.reject(new Error('Something failed')));

        server.inject(options, (response) => {
          expect(response.statusCode).to.equal(500);
          done();
        });
      });
    });

    describe('Query parameters', () => {
      it('Should be passed to the database call', (done) => {
        findUserStub.returns(Promise.resolve(validDBData));

        server.inject(options, () => {
          sinon.assert.calledOnce(findUserStub);
          expect(findUserStub.calledWith({ id: 1 })).to.be.true;
          done();
        });
      });
    });
  });
});
