const server = require('../../server.js');
const expect = require('chai').expect;
const sinon = require('sinon');

const db = require('../../repositories');

describe('Top active users resource', () => {
  describe('GET /topActiveUsers', () => {
    const options = {
      method: 'GET',
      url: '/topActiveUsers'
    };

    const validDBData = [{
      id: 1,
      created_at: '2016-01-01T15:30:00.000Z', name: 'Jorge',
      count: 1,
      listings: [ { id: 1, name: 'My listing' } ]
    }];

    let getTotalActiveUsersStub;

    beforeEach(() => {
      getTotalActiveUsersStub = sinon.stub(db.topActiveUsers, 'search');
    });

    afterEach(() => {
      getTotalActiveUsersStub.restore();
    });

    describe('When request is successful', () => {
      it('Should return a HTTP 200 code', (done) => {
        getTotalActiveUsersStub.returns(Promise.resolve(validDBData));

        server.inject(options, (response) => {
          expect(response.statusCode).to.equal(200);
          done();
        });
      });

      it('Should return an array', (done) => {
        getTotalActiveUsersStub.returns(Promise.resolve(validDBData));

        server.inject(options, (response) => {
          expect(response.result).to.be.instanceof(Array);
          done();
        });
      });
    });

    describe('When an error is thrown', () => {
      it('Should return a HTTP 500 code', (done) => {
        getTotalActiveUsersStub.returns(Promise.reject(new Error('Something failed')));

        server.inject(options, (response) => {
          expect(response.statusCode).to.equal(500);
          done();
        });
      });
    });

    describe('Query parameters', () => {
      it('Should be passed to the database call', (done) => {
        getTotalActiveUsersStub.returns(Promise.resolve(validDBData));
        const optionsAndCriteria = Object.assign({}, options, { url: '/topActiveUsers?page=2' });

        server.inject(optionsAndCriteria, () => {
          sinon.assert.calledOnce(getTotalActiveUsersStub);
          expect(getTotalActiveUsersStub.calledWith({ pageNumber: 2 })).to.be.true;
          done();
        });
      });
    });

    describe('When pagination parameter is not passed', () => {
      it('Should return the first page by default', (done) => {
        getTotalActiveUsersStub.returns(Promise.resolve(validDBData));

        server.inject(options, () => {
          sinon.assert.calledOnce(getTotalActiveUsersStub);
          expect(getTotalActiveUsersStub.calledWith({ pageNumber: 1 })).to.be.true;
          done();
        });
      });
    });
  });
});
